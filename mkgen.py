'''
Markov Chains based text generator
'''

import numpy as np
import os
import glob
from random import randint

class Speech:
    
    def __init__(self, n, lan):
        self.words = []
        path =  "%s/txt/%s" % (os.path.dirname(os.path.realpath(__file__)),lan)
        for filename in glob.glob(os.path.join(path, '*.txt')):
            with open(filename, "r") as file:
                original = file.read()
                self.words += original.split()
        self.n = n

    def make_pairs(self):
        for i in range(len(self.words)-2):
            yield ((self.words[i], self.words[i+1]), self.words[i+2])
        
    def create_dict(self):
        self._dict = {}
        pairs = self.make_pairs()
        for key, word in pairs:
            if key in self._dict.keys():
                self._dict[key].append(word)
            else:
                self._dict[key] = [word]

    def select_first(self):
        i = randint(0, len(self.words) - 3)
        self.speech = [ self.words[i], self.words[i+1] ]

    def say(self):
        self.create_dict()
        self.select_first()
        for i in range(self.n):
            self.speech.append(
                np.random.choice(
                    self._dict[(self.speech[-2],self.speech[-1])]))

        return ' '.join(self.speech)

if __name__ == "__main__":

    print (Speech(100, "es").say())
